# When we add our words to the index, we don't really want to include
# html tags such as <body>, <head>, <table>, <a href="..."> and so on.

# Write a procedure, remove_tags, that takes as input a string and returns
# a list of words, in order, with the tags removed. Tags are defined to be
# strings surrounded by < >. Words are separated by whitespace or tags. 
# You may assume the input does not include any unclosed tags, that is,  
# there will be no '<' without a following '>'.

def remove_tags(page):
    start_point = page.find('>')
    end_point = page.find('<', start_point)
    keywords = page[start_point + 1 : end_point]
    return keywords,end_point
   
def all_keyword(page):
    index = []
    while True:
        keywords,end_point = remove_tags(page)
        if keywords:
            index.append(keywords)
            #print index 
            page = page[end_point:] 
            #print page  
        else:
            break

    return index
    
def split_keywords(page):
    list = []
    index = all_keyword(page)
    for e in index:
       #print e
        value = e.split()
        list = list + value
    print list

# Sampel Inputs:

print split_keywords('''<h1>Title</h1> <p>This is a
                    <a href="http://www.ucodesoft.com">link</a>.<p>''')
#>>> ['Title','This','is','a','link','.']

print split_keywords('''<table cellpadding='3'>
                     <tr> <td>Hello</td> <td>World!</td> </tr>
                     </table>''')
#>>> ['Hello','World!']

print split_keywords("<hello> <goodbye>")
#>>> []

print split_keywords("This is plain text.")
#>>> ['This', 'is', 'plain', 'text.']
